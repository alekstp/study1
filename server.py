# при сдаче проверка проходит только при запущенном в консоли скрипте и на python2
import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('0.0.0.0', 2222))
s.listen(10)

while True:
    con = s.accept()
    while True:
        data = con.recv(1024)
        if not data or data == 'close' : break
        con.send(data)
    con.close()